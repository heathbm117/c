﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    public class EmployeeComparer : IEqualityComparer<Employee>
    {
        public bool Equals(Employee x, Employee y)
        {
            return x.Name == y.Name && x.ID == y.ID;
        }

        public int GetHashCode(Employee obj)
        {
            return obj.Name.GetHashCode() ^ obj.ID.GetHashCode();
        }
    }
}
