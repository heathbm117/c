﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    public class Employee
    {
        public int ID;
        public string Name;
        public int DeptID;
        public string Gender;
        public List<int> nums = new List<int>() { 1, 2, 3 };

        public static List<Employee> GetAll()
        {
            return new List<Employee>
            {
                new Employee
                {
                    Name = "heath",
                    ID = 1,
                    DeptID = 1,
                    Gender= "Male"
                },
                new Employee
                {
                    Name = "joe",
                    ID = 2,
                    DeptID = 1,
                    Gender= "Male"
                },
                new Employee
                {
                    Name = "louise",
                    ID = 3,
                    DeptID = 2,
                    Gender= "Female"
                },
                new Employee
                {
                    Name = "Jim",
                    ID = 4,
                    DeptID = 2,
                    Gender= "Male"
                },
                new Employee
                {
                    Name = "Claire",
                    ID = 5,
                    DeptID = 1,
                    Gender= "Female"
                },
                new Employee
                {
                    Name = "Anna",
                    ID = 6,
                    DeptID = 2,
                    Gender= "Female"
                },
                new Employee
                {
                    Name = "Jessie",
                    ID = 7,
                    DeptID = 2,
                    Gender= "Male"
                },
                new Employee
                {
                    Name = "Amy",
                    ID = 8,
                    DeptID = 2,
                    Gender= "Female"
                },
                new Employee
                {
                    Name = "Amy",
                    ID = 9,
                    Gender= "Female"
                }
            };
        }

        //public override bool Equals(object obj)
        //{
        //    return this.Name == ((Employee)obj).Name && this.ID == ((Employee)obj).ID;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Name.GetHashCode() ^ this.ID.GetHashCode();
        //}
    }

    public class Dept
    {
        public int ID;
        public string Name;

        public static List<Dept> GetAll()
        {
            return new List<Dept>
            {
                new Dept
                {
                    ID = 1,
                    Name = "IT"
                },
                new Dept
                {
                    ID = 2,
                    Name = "Sales"
                },
                new Dept
                {
                    ID = 3,
                    Name = "Business"
                }
            };
        }
    }
}
