﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            LINQ_Stuff.linqFunc();
            Console.ReadLine();
        }

    }

    public static class LINQ_Stuff
    {

        public static void linqFunc()
        {
            // Select returns all (use to transform)
            // Where filters down
            // Use as enumerable to move processing to the sql to objects
            // Use toList() after select, where, take, skip to execute immediately as they are delayed

            doubleIt dblIt = x => x * 2; // Change delegate code on the go
            Console.WriteLine($"5 * 2 = {dblIt(5)}"); // invoke
            dblIt.Invoke(2); // invoke

            Console.WriteLine();

            List<int> numList = new List<int> { 1, 9, 2, 6, 3 };

            numList.First(a => a % 2 == 0); // first even

            var evenList = numList.Where(a => a % 2 == 0).ToList(); // all even

            foreach (var item in evenList)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var rangeList = numList.Where(x => x > 2 && x < 9).ToList(); // select range

            foreach (var item in rangeList)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            List<int> flipList = new List<int>();

            int i = 0;
            Random rand = new Random();

            while (i < 100)
            {
                flipList.Add(rand.Next(1, 3));  // inclusive and exclusive
                i++;
            }

            Console.WriteLine("Heads : {0}", flipList.Where(a => a == 1).ToList().Count);

            Console.WriteLine();

            var nameList = new List<string> { "Doug", "Sally", "Sue" };

            var sNameList = nameList.Where(x => x.StartsWith("S")); // make a list with names that start with s // where is about filtering

            foreach (var item in sNameList)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var onTo10 = new List<int>();

            onTo10.AddRange(Enumerable.Range(1, 10));

            var squares = onTo10.Select(x => x * x); // squares all elements // Select is about transforming

            foreach (var item in squares)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var listOne = new List<int> { 1, 3, 4 };

            var listTwo = new List<int> { 4, 6, 8, 9 }; // the extra 9 is ignored

            var sunList = listOne.Zip(listTwo, (x, y) => x + y).ToList(); // cobines the list adding the values

            foreach (var item in sunList)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var numList2 = new List<int>() { 1, 2, 3, 4, 5 };

            // Sum (Long way)

            Console.WriteLine("Sum : {0} ", numList2.Aggregate((a, b) => a + b)); // sum of the list

            Console.WriteLine();

            // Sum (Easy way)

            Console.WriteLine(numList2.Sum());

            Console.WriteLine();

            var numList3 = new List<int>() { 1, 2, 3, 4, 5 };

            // Average

            Console.WriteLine("Avg : {0}", numList3.AsQueryable().Average()); // get the average of the list

            Console.WriteLine();

            // All

            Console.WriteLine("All > 3 : {0}", numList3.All(x => x > 3)); // are all abouve false

            Console.WriteLine();

            // Any

            Console.WriteLine("Any > 3 : {0}", numList3.Any(x => x > 3)); // are any abouve false

            Console.WriteLine();

            var numList4 = new List<int>() { 1, 2, 3, 2, 3 };

            // Distinct (All but no duplicates)

            Console.WriteLine("Distinct 3 : {0}", string.Join(", ", numList4.Distinct()));

            Console.WriteLine();

            var numList5 = new List<int>() { 1, 2, 3, 2, 3 };

            var numList6 = new List<int>() { 3 };

            // Except (Missing from one)

            Console.WriteLine("Except : {0} ", string.Join(", ", numList5.Except(numList6))); // NOT in both

            Console.WriteLine();

            // Intersect (Common between both)

            Console.WriteLine("Intersect : {0} ", string.Join(", ", numList5.Intersect(numList6))); // IN both

            // SelectMany

            List<List<string>> groups = new List<List<string>>() { new List<string> { "a", "b" }, new List<string> { "c", "d" } };

            var allGroups = groups.SelectMany(x => x).First(x => x.Equals("a"));
            Console.WriteLine(allGroups); // shows a

            List<int> nums = new List<int> { 1, 2, 3, 4, 5 };
            List<int> nums2 = new List<int> { 6, 7, 8, 9, 10 };

            // Deeper

            List<Employee> moedlList = new List<Employee> { new Employee { ID = 1, Name = "" } };

            var anonMore = moedlList.SelectMany(grps => grps.nums, (IndividualVariable, FieldSelectedFromIndividualVariable) => new { a = FieldSelectedFromIndividualVariable, b = IndividualVariable.Name });

            // .Distinct() removes duplicated

            // shortest string

            List<string> countries = new List<string> { "USA", "Caanda", "UK" };

            int smallest = countries.Min(x => x.Length); // Number of characters in smallest string

            // Indexes

            string word = "hello";

            int firstInd = word.IndexOf("h");
            int lastInd = word.LastIndexOf("o");

            // Agregate

            var aggregatedCountries = countries.Aggregate((a, b) => a + "," + b);
            Console.WriteLine(aggregatedCountries);

            // Func's are generic delegates

            Func<int, string> projection = x => "Value=" + x;
            int[] values = { 3, 7, 10 };
            var strings = values.Select(projection);

            foreach (string s in strings)
            {
                Console.WriteLine(s);
            }

            // select transform

            var transformedNums = nums.Select((number, index) => new { ind = index, nub = number });
            Console.WriteLine(transformedNums.First().ind); // Indexes and numbers

            nums.Select(x => x);

            // Deeper

            var result = nums.Select((number, index) => new { ind = index, nub = number }).Where(x => x.nub % 2 == 0).Select(x => x.ind);
            Console.WriteLine(result.First()); // Index of first even number 

            // order

            var orderedList = Employee.GetAll().OrderBy(s => s.Name);
            foreach (var m in orderedList)
            {
                Console.WriteLine(m.Name);
            }


            // order descding

            var orderedListDesc = Employee.GetAll().OrderByDescending(s => s.Name);
            foreach (var m in orderedListDesc)
            {
                Console.WriteLine(m.Name);
            }

            // order by then by

            var orderedLisThen = Employee.GetAll().OrderBy(s => s.Name).ThenBy(s => s.ID);
            foreach (var m in orderedLisThen)
            {
                Console.WriteLine(m.Name);
            }

            // reverse 

            var reveresed = orderedLisThen.Reverse();

            // take

            Console.WriteLine();

            var takeList = nameList.Take(2);
            foreach (var m in takeList)
            {
                Console.WriteLine(m);
            }

            // skip

            Console.WriteLine();

            var skipList = nameList.Skip(2);
            foreach (var m in skipList)
            {
                Console.WriteLine(m);
            }

            // take while list

            Console.WriteLine();

            var takeWhileList = nameList.TakeWhile(s => s.Length > 1);
            foreach (var m in takeWhileList)
            {
                Console.WriteLine(m);
            }

            // skip while list

            Console.WriteLine();

            var skipWhileList = nameList.TakeWhile(s => s.Length > 1);
            foreach (var m in skipWhileList)
            {
                Console.WriteLine(m);
            }

            // To dictionary defined

            Dictionary<int, string> dict = Employee.GetAll().ToDictionary(x => x.ID, x => x.Name);

            // To dictionary whole

            Dictionary<int, Employee> dict2 = Employee.GetAll().ToDictionary(x => x.ID);
            string nameOfFirst = dict2.First().Value.Name;

            // to lookup (like dictionary, but can have same keys and always returns IEnumerable)

            ILookup<int, Employee> look = Employee.GetAll().ToLookup(x => x.ID);
            var test = look[1];
            nameOfFirst = test.First().Name;

            // cast STRICT will crash

            ArrayList items = new ArrayList();
            items.Add(1);
            items.Add(2);
            items.Add(3);

            IEnumerable<int> IntList = items.Cast<int>();

            Console.WriteLine();
            foreach (int ii in IntList)
            {
                Console.WriteLine(ii);
            }

            // of type

            items.Add("4");

            IEnumerable<int> IntList2 = items.OfType<int>();

            Console.WriteLine();
            foreach (int ii in IntList2)
            {
                Console.WriteLine(ii);
            }

            // groupBy

            var groups5 = Employee.GetAll().GroupBy(x => x.DeptID);

            foreach (var grp5 in groups5)
            {
                Console.WriteLine(grp5.Key + ": " + grp5.Count());
            }

            // special count

            Console.WriteLine();

            int numOfHeaths = Employee.GetAll().Count(x => x.Name.Equals("heath"));
            Console.WriteLine(numOfHeaths);

            // groupBy Max

            Console.WriteLine();

            foreach (var grp5 in groups5)
            {
                Console.WriteLine(grp5.Key + ": " + grp5.Max(x => x.ID)); // get max id of each group
            }

            // groupby multiple keys

            var complexGroup = Employee.GetAll().GroupBy(x => new { x.Gender, x.DeptID }) // 
                                         .OrderBy(x => x.Key.Gender).ThenBy(x => x.Key.DeptID)
                                         .Select(x => new
                                         {
                                             gend = x.Key.Gender,
                                             grp = x.Key.DeptID,
                                             model = x.OrderBy(g => g.ID)
                                         });

            Console.WriteLine();

            foreach (var one in complexGroup)
            {
                Console.WriteLine("Gender is : " + one.gend + " and group is " + one.grp);

                foreach (Employee mdl in one.model)
                {
                    Console.WriteLine(mdl.Name + " is a " + one.gend);
                }

                Console.WriteLine("-----");
            }

            // group Join (Outer, can contain empty)

            Console.WriteLine("GROUP JOIN");

            var groupedJoinList = Dept.GetAll().GroupJoin(Employee.GetAll(),
                                                          x => x.ID,
                                                          x => x.DeptID,
                                                          (dept, employ) => new
                                                          {
                                                              Department = dept,
                                                              Employees = employ
                                                          });

            foreach (var dept in groupedJoinList)
            {
                Console.WriteLine("Dept = " + dept.Department.Name);
                Console.WriteLine();

                foreach (var employee in dept.Employees)
                {
                    Console.WriteLine(employee.Name + " works in " + employee.DeptID);
                }

                Console.WriteLine("-----");
            }


            // group InnerJoin (FLAT, will only show matching elements)

            Console.WriteLine("INNER JOIN");

            var groupedInnerJoinList = Dept.GetAll().Join(Employee.GetAll(),
                                                          x => x.ID,
                                                          x => x.DeptID,
                                                          (dept, employ) => new
                                                          {
                                                              Department = dept,
                                                              EmployeName = employ.Name
                                                          });

            foreach (var dept in groupedInnerJoinList)
            {
                Console.WriteLine(dept.EmployeName + " works in " + dept.Department.Name);
            }

            // LEFT group Join (The right can have empty)

            Console.WriteLine("LEFT GROUP JOIN");
            Console.WriteLine();

            var groupedLeftJoinList = Employee.GetAll().GroupJoin(Dept.GetAll(),
                                                          x => x.DeptID,
                                                          x => x.ID,
                                                          (employ, dept) => new
                                                          {
                                                              Department = dept,
                                                              Employee = employ
                                                          }).
                                                          SelectMany(x => x.Department.DefaultIfEmpty(),
                                                          (a, b) => new
                                                          {
                                                              EmployeeName = a.Employee.Name,
                                                              DeptName = b == null ? "No Name" : b.Name
                                                          }
                                                          );

            foreach (var dept in groupedLeftJoinList)
            {
                Console.WriteLine(dept.EmployeeName + " works in " + dept.DeptName);
            }

            // cross join, cartesian prodcuct with select many

            Console.WriteLine();
            var crossJoined = Dept.GetAll().SelectMany(e => Employee.GetAll(), (d, e) => new { d, e });

            foreach (var crossed in crossJoined)
            {
                Console.WriteLine(crossed.d.Name + " " + crossed.e.Name);
            }

            // cross join, cartesian prodcuct with join

            Console.WriteLine();
            var crossJoined2 = Dept.GetAll().Join(Employee.GetAll(),
                                                 d => true,
                                                 e => true,
                                                 (d, e) => new { d, e });

            foreach (var crossed in crossJoined)
            {
                Console.WriteLine(crossed.d.Name + " " + crossed.e.Name);
            }

            // distinct

            string[] countriesArray = { "USA", "usa", "UK", "CANADA" };

            var distinct = countriesArray.Distinct();
            var distinctCase = countriesArray.Distinct(StringComparer.OrdinalIgnoreCase); // cases will not be looked at

            // distinct objects

            List<Employee> emps = new List<Employee>
            {
                new Employee
                {
                    Name = "Jim",
                    ID = 1
                },
                new Employee
                {
                    Name = "Jim",
                    ID = 1
                },
                new Employee
                {
                    Name = "Jane",
                    ID = 2
                }
            };

            var dist1 = emps.Distinct(); // method 1 = if equals() and gethashcode() are overriden, this will be correct. If not it will compare object references
            var dist2 = emps.Distinct(new EmployeeComparer()); // method 2
            var dist3 = emps.Select(x => new { x.Name, x.ID }).Distinct(); // method 3 easiest

            Console.WriteLine("first = " + dist1.Count() + " second = " + dist2.Count() + " third = " + dist3.Count());

            // exists

            var dist44 = emps.Exists(x => x.ID == 1 && x.Name == "Jim"); // true
            
            // contains: override equals and tohashcode OR implement IEqualityComparer

            // union, combine arrays and remove duplicates

            int[] int1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] int2 = { 1, 3, 5, 7, 9, 13, 15, 17, 19 };

            var combinedIntArray = int1.Union(int2);

            Console.WriteLine();
            foreach (var el in combinedIntArray)
            {
                Console.WriteLine(el);
            }

            // concat, combine arrays and does not remove duplicates

            var combinedIntArray2 = int1.Concat(int2);

            Console.WriteLine();
            foreach (var el in combinedIntArray2)
            {
                Console.WriteLine(el);
            }

            // union with objects

            List<Employee> emps5 = new List<Employee>
            {
                new Employee
                {
                    Name = "Mike",
                    ID = 1
                },
                new Employee
                {
                    Name = "Jim",
                    ID = 2
                }
            };

            List<Employee> emps6 = new List<Employee>
            {
                new Employee
                {
                    Name = "Mike",
                    ID = 1
                },
                new Employee
                {
                    Name = "Joe",
                    ID = 3
                }
            };

            var unionedObjects = emps5.Union(emps6);

            Console.WriteLine(unionedObjects.Count()); // 4

            var unionedObjects2 = emps5.Select(x => new { x.Name, x.ID }).Union(emps6.Select(x => new { x.Name, x.ID }));

            Console.WriteLine(unionedObjects2.Count()); // 3

            // intersect (only common elements)

            var unionedObjects3 = emps5.Select(x => new { x.Name, x.ID }).Intersect(emps6.Select(x => new { x.Name, x.ID }));

            Console.WriteLine(unionedObjects3.Count()); // 1

            // range

            Console.WriteLine();

            List<int> rangeOfInts = Enumerable.Range(1, 10).ToList();

            foreach (int ints in rangeOfInts)
            {
                Console.WriteLine(ints);
            }

            // range where even numbers

            Console.WriteLine();

            List<int> rangeOfInts2 = Enumerable.Range(1, 10).Where(x => x % 2 == 0).ToList();

            foreach (int ints in rangeOfInts2)
            {
                Console.WriteLine(ints);
            }


            // repeat

            Console.WriteLine();

            List<string> repeated = Enumerable.Repeat("hello", 5).ToList();

            foreach (string words in repeated)
            {
                Console.WriteLine(words);
            }

            // enumerable empty
            Console.WriteLine();

            List<int> nums10 = null;
            nums10 = nums10 ?? Enumerable.Empty<int>().ToList();

            foreach (int num in nums10)
            {
                Console.WriteLine(num);
            }

            // sequence equal

            var emp22 = Employee.GetAll();
            var emp44 = Employee.GetAll();

            bool resulte = emp22.SequenceEqual(emp44);
            bool resulte2 = emp22.Select(x => new { x.ID }).SequenceEqual(emp44.Select(x => new { x.ID }));

            Console.WriteLine(resulte + " and " + resulte2);

        }

        public delegate double doubleIt(double val);

    }
}
