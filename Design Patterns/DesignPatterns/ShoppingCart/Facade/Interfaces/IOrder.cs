﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Interfaces
{
    public interface IOrder
    {
        int PlaceOrderDetails(int cartID, int shippingAddressID);
    }
}
