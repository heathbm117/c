﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Interfaces
{
    public interface IWallet
    {
        double GetUserBalance(int userID);
    }
}
