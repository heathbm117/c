﻿using ShoppingCart.Structural.Facade.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Interfaces
{
    public interface ICart
    {
        Product GetItemDetails(int itemID);
        bool CheckItemAvailability(Product product);
        bool LockItemInStock(int itemID, int quantity);
        int AddItemToCart(int itemID, int quantity);
        double GetCartPrice(int cartID);
    }
}
