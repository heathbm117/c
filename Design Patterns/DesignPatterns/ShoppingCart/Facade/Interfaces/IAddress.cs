﻿using ShoppingCart.Structural.Facade.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Interfaces
{
    public interface IAddress
    {
        Address GetAddressDetails(int userID);
    }
}
