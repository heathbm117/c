﻿using ShoppingCart.Structural.Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Implementation
{
    public class AddressDetails : IAddress
    {
        public Models.Address GetAddressDetails(int userID)
        {
            Console.WriteLine("\t SubSystem Address : GetAddressDetails");
            return new Models.Address();
        }

    }
}
