﻿using ShoppingCart.Structural.Facade.Interfaces;
using System;

namespace ShoppingCart.Structural.Facade.Implementation
{
    public class Wallet : IWallet
    {
        public double GetUserBalance(int userID)
        {
            Console.WriteLine("\t SubSystem Wallet : GetUserBalance");
            return 100;
        }
    }
}