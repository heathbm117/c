﻿using ShoppingCart.Structural.Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Implementation
{
    public class Order : IOrder
    {
        public int PlaceOrderDetails(int cartID, int shippingAddressID)
        {
            Console.WriteLine("\t SubSystem Order : PlaceOrderDetails");
            return 10;
        }
    }
}
