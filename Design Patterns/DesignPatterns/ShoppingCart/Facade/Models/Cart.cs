﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCart.Structural.Facade.Models
{
    public class Cart
    {
        public int CartID { get; set; }
        public int UserID { get; set; }
        public IEnumerable<CartItem> ShoppingCart { get; set; }
    }
}
