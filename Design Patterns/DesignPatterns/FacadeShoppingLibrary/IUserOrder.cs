﻿using System;

namespace FacadeShoppingLibrary
{
    public interface IUserOrder
    {
        int AddToCart(int itemID, int qty);
        int PlaceOrder(int cartID, int userID);
    }
}
