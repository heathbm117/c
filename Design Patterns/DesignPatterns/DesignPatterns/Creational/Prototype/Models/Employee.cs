﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace DesignPatterns.Creational.Prototype.Models
{
    public class Employee : ICloneable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int DepartmentID { get; set; }
        public Address AddressDetails { get; set; }

        public override string ToString()
        {
            return string.Format(" Name : {0}  " + "DepartmentID : {1}  {2} ",
                this.Name, this.DepartmentID.ToString(), AddressDetails.ToString());
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public Employee DeepCopy()
        {
            using (var stream = new MemoryStream())
            {
                var serializer = new XmlSerializer(typeof(Employee));

                serializer.Serialize(stream, this);
                stream.Position = 0;
                return (Employee)serializer.Deserialize(stream);
            }
        }
    }
}
