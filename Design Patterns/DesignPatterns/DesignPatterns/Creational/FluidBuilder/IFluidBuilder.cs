﻿using DesignPatterns.Creational.Builder.Models;

namespace DesignPatterns.Creational.Builder
{
    interface IFluidBuilder
    {
        FluidBuilder WithType(string type); 
        FluidBuilder ForCompany(string  compamy);
        FluidBuilder AtAddress(string address);
        FluidBuilder WithCost(int cost);
        FluidBuilder WithQuantity(int quantity);

        PurchaseOrder BuildPurchaseOrder();
    }
}
