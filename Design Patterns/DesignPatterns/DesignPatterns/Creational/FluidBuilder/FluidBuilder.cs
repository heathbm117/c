﻿using DesignPatterns.Creational.Builder.Models;
using System;

namespace DesignPatterns.Creational.Builder
{
    public class FluidBuilder : IFluidBuilder
    {
        private string _type;
        private DateTime _createdOn = DateTime.Now;
        private string _companyName;
        private string _address;
        private int _cost;
        private int _quantity;

        public FluidBuilder AtAddress(string address)
        {
            _address = address;
            return this;
        }

        public FluidBuilder WithCost(int cost)
        {
            _cost = cost;
            return this;
        }

        public FluidBuilder ForCompany(string compamy)
        {
            _companyName = compamy;
            return this;
        }

        public FluidBuilder WithQuantity(int quantity)
        {
            _quantity = quantity;
            return this;
        }

        public FluidBuilder WithType(string type)
        {
            _type = type;
            return this;
        }


        public PurchaseOrder BuildPurchaseOrder()
        {
            return new PurchaseOrder(_type)
            {
                CreatedOn = _createdOn,
                Address = _address,
                CompanyName = _companyName,
                Cost = _cost,
                Quantity = _quantity
            };
        }

        public static implicit operator PurchaseOrder(FluidBuilder builder)
        {
            return builder.BuildPurchaseOrder();
        }
    }
}
