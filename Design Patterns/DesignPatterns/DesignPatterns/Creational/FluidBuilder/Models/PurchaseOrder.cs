﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Creational.Builder.Models
{
    public class PurchaseOrder
    {
        private string _type;

        public PurchaseOrder(string type)
        {
            _type = type;
        }

        public DateTime CreatedOn { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public int Cost { get; set; }
        public int Quantity { get; set; }

        public int TotalCost => Cost * Quantity;

        public void PrintPurchaseOrder()
        {
            // logic
        }
    }
}
