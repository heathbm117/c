﻿using System;

namespace DesignPatterns.Creational
{
    public sealed class LazySingleton // Thread safe by default
    {
        private LazySingleton() { }

        private static readonly Lazy<LazySingleton> instance = new Lazy<LazySingleton>(() => new LazySingleton());

        public static LazySingleton Intance
        {
            get
            {
                return instance.Value;
            }
        }
    }
}