﻿namespace DesignPatterns.Creational
{
    public sealed class EagerSingleton // Thread safe by default
    {
        private EagerSingleton() { }

        private static readonly EagerSingleton instance = new EagerSingleton();

        public static EagerSingleton Intance
        {
            get
            {
                return instance;
            }
        }
    }
}