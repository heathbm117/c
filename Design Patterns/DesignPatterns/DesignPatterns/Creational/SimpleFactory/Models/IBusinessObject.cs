﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Models
{
    public interface IBusinessObject
    {
        void Function();
    }
}
