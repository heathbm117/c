﻿using DesignPatterns.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Creational
{
    public class Factory
    {
        public IBusinessObject Create(bool decision)
        {
            if (decision)
            {
                return new BusinessObject();
            }
            else
            {
                return new BusinessObject2();
            }
        }
    }
}
