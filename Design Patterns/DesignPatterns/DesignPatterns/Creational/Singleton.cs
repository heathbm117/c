﻿namespace DesignPatterns.Creational
{
    public sealed class Singleton
    {
        private Singleton() { }

        private static Singleton instance = null;
        private static readonly object Lock = new object();

        public static Singleton Intance
        {
            get
            { 
                if (instance == null)
                    lock (Lock) // Tread safe
                        if (instance == null) // Double check locking
                            instance = new Singleton();

                return instance;
            }
        }
    }
}