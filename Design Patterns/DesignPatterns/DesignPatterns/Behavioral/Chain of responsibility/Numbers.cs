﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Chain_of_responsibility
{
    public class Numbers
    {
        private int num1, num2;

        private string calculationWanted;

        public Numbers(int newNumber1, int newNumber2, string calcWanted)
        {
            num1 = newNumber1;
            num2 = newNumber2;
            calculationWanted = calcWanted;
        }

        public int GetNumb1()
        {
            return num1;
        }

        public int GetNumb2()
        {
            return num2;
        }

        public string getCalcWanted()
        {
            return calculationWanted;
        }
    }
}
