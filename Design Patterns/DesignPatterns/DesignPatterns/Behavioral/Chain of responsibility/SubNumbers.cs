﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Chain_of_responsibility
{
    class SubNumbers : IChain
    {
        private IChain nextInChain;

        public void Calculate(Numbers request)
        {
            if (request.getCalcWanted().Equals("sub"))
            {
                Console.WriteLine(request.GetNumb1() - request.GetNumb2());
            }
            else
            {
                nextInChain.Calculate(request);
            }
        }

        public void SetNextChain(IChain nextChain)
        {
            this.nextInChain = nextChain;
        }
    }
}
