﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Chain_of_responsibility
{
    public interface IChain
    {
        void SetNextChain(IChain nextChain);
        void Calculate(Numbers request);
    }
}
