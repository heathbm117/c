﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Interpretor
{
    class NonterminalExpression : AbstractExpression

    {
        public override void Interpret(Context context)
        {
            Console.WriteLine("Called Nonterminal.Interpret() " + context.word);
        }
    }
}
