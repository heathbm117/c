﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Interpretor
{
    abstract class AbstractExpression

    {
        public abstract void Interpret(Context context);
    }
}
