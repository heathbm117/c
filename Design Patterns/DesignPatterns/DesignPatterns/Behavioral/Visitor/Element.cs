﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Visitor
{
    abstract class Element

    {
        public abstract void Accept(Visitor visitor);
    }
}
