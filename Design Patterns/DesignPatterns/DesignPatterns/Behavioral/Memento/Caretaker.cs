﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Memento
{
    class Caretaker
    {
        private Memento _memento;

        // Gets or sets memento

        public Memento Memento
        {
            set { _memento = value; }
            get { return _memento; }
        }
    }
}
