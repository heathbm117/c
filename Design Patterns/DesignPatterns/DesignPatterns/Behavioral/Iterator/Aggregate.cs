﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Iterator
{
    abstract class Aggregate

    {
        public abstract Iterator CreateIterator();
    }
}
