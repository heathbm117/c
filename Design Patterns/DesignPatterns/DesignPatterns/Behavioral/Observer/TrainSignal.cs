﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Observer
{
    class TrainSignal
    {
        public Action TrainsAComing;

        public void HereComesATrain()
        {
            // logic
            TrainsAComing();
        }
    }
}
