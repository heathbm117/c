﻿using System;

namespace DesignPatterns.Behavioral.Observer
{
    class Car
    {
        public Car(TrainSignal trainSignal)
        {
            trainSignal.TrainsAComing += StopTheCar;
        }

        void StopTheCar()
        {
            Console.WriteLine("Breaking!");
        }
    }
}