﻿namespace DesignPatterns.Behavioral.Command
{
    abstract class Command2

    {
        protected Receiver receiver;

        // Constructor

        public Command2(Receiver receiver)
        {
            this.receiver = receiver;
        }

        public abstract void Execute();
    }
}
