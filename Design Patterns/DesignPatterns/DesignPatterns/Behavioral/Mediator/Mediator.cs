﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral.Mediator
{
    abstract class Mediator

    {
        public abstract void Send(string message,
          Colleague colleague);
    }
}
