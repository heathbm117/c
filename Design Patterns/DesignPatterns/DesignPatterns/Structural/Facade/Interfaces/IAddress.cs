﻿using DesignPatterns.Creational.Prototype.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Facade.Interfaces
{
    public interface IAddress
    {
        Address GetAddressDetails(int userID);
    }
}
