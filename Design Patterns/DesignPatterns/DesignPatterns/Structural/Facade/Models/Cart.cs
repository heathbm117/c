﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Facade.Models
{
    public class Cart
    {
        public int CartID { get; set; }
        public int UserID { get; set; }
        public IEnumerable<CartItem> ShoppingCart { get; set; }
    }
}
