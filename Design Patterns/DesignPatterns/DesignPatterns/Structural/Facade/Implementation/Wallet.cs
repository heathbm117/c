﻿using DesignPatterns.Structural.Facade.Interfaces;
using System;

namespace DesignPatterns.Structural.Facade.Implementation
{
    public class Wallet : IWallet
    {
        public double GetUserBalance(int userID)
        {
            Console.WriteLine("\t SubSystem Wallet : GetUserBalance");
            return 100;
        }
    }
}