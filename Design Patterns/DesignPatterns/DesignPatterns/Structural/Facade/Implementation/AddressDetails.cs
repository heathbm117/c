﻿using DesignPatterns.Structural.Facade.Interfaces;
using System;

namespace DesignPatterns.Structural.Facade.Implementation
{
    public class AddressDetails : IAddress
    {
        public Creational.Prototype.Models.Address GetAddressDetails(int userID)
        {
            Console.WriteLine("\t SubSystem Address : GetAddressDetails");
            return new Creational.Prototype.Models.Address();
        }
    }
}