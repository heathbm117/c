﻿using DesignPatterns.Structural.Composite.Component;
using System;

namespace DesignPatterns.Structural.Composite.Leaf
{
    public class Employee3 : IEmployee3
    {
        public string Name { get; set; }
        public string Department { get; set; }

        public Employee3(string name, string dept)
        {
            Name = name;
            Department = dept;
        }

        public void GetDetails(int indentation)
        {
            Console.WriteLine(string.Format("{0}- Name:{1}, Dept:{2} (Leaf) ",
                new String('-', indentation), this.Name.ToString(),
                this.Department));
        }
    }
}