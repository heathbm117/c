﻿namespace DesignPatterns.Structural.Composite.Component
{
    public interface IEmployee3
    {
        void GetDetails(int indentation);
    }
}