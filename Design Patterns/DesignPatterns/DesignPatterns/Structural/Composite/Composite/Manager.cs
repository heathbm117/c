﻿using DesignPatterns.Structural.Composite.Component;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Composite.Composite
{
    public class Manager : IEmployee3
    {
        public List<IEmployee3> SubOrdinates;

        public Manager(string name, string dept)
        {
            this.Name = name;
            this.Department = dept;
            SubOrdinates = new List<IEmployee3>();
        }

        public string Name { get; set; }
        public string Department { get; set; }

        public void GetDetails(int indentation)
        {
            Console.WriteLine();
            Console.WriteLine(string.Format("{0}+ Name:{1}, " +
                "Dept:{2} - Manager(Composite)",
                new String('-', indentation), this.Name.ToString(),
                this.Department));
            foreach (IEmployee3 component in SubOrdinates)
            {
                component.GetDetails(indentation + 1);
            }
        }
    }
}