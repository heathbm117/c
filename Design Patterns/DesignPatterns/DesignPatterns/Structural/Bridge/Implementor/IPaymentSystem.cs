﻿namespace DesignPatterns.Structural.Bridge.Models
{
    public interface IPaymentSystem
    {
        void ProcessPayment(string paymentSystem);
    }
}