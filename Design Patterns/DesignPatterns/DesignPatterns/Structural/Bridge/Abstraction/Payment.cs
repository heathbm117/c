﻿using DesignPatterns.Structural.Bridge.Models;

namespace DesignPatterns.Structural.Bridge
{
    public abstract class Payment
    {
        public IPaymentSystem _IPaymentSystem;
        public abstract void MakePayment();
    }
}
