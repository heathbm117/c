﻿using System;

namespace DesignPatterns.Structural.Bridge.Models
{
    public class CitiPaymentSystem : IPaymentSystem
    {
        public void ProcessPayment(string paymentSystem)
        {
            Console.WriteLine("Using CITI bank with " + paymentSystem);
        }
    }
}