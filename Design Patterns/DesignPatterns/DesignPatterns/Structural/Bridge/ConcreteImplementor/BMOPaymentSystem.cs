﻿using System;

namespace DesignPatterns.Structural.Bridge.Models
{
    public class BMOPaymentSystem : IPaymentSystem
    {
        public void ProcessPayment(string paymentSystem)
        {
            Console.WriteLine("Using BMO bank with " + paymentSystem);
        }
    }
}
