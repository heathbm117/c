﻿namespace DesignPatterns.Structural.Proxy
{
    interface IATM_Machine
    {
        int GetCashBalance();
        void Take20();
    }
}
