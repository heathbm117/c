﻿namespace DesignPatterns.Structural.Proxy
{
    class ProxyATM_Machine : IATM_Machine
    {
        ATM_Machine ATM = new ATM_Machine();

        public int GetCashBalance()
        {
            return ATM.GetCashBalance();
        }

        public void Take20()
        {
            ATM.Take20();
        }
    }
}