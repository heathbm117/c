﻿namespace DesignPatterns.Structural.Proxy
{
    class ATM_Machine : IATM_Machine
    {
        int Amount = 1000;

        public void SetCashInMachine(int amount)
        {
            Amount -= amount;
        }

        public void Take20()
        {
            SetCashInMachine(20);
        }

        public int GetCashBalance()
        {
            return Amount;
        }
    }
}