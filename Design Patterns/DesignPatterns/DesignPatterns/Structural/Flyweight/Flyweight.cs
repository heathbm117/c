﻿namespace DesignPatterns.Structural.Flyweight
{
    abstract class Flyweight
    {
        public abstract void Operation(int extrinsicstate);
    }
}
