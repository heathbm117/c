﻿using System;

namespace DesignPatterns.Structural.Flyweight
{
    class UnsharedConcreteFlyweight : Flyweight

    {
        public override void Operation(int extrinsicstate)
        {
            Console.WriteLine("UnsharedConcreteFlyweight: " +
              extrinsicstate);
        }
    }
}
