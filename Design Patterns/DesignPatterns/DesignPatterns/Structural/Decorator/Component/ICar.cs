﻿namespace DesignPatterns.Structural.Decorator.Component
{
    public interface ICar
    {
        string Make { get; }
        double GetPrice();
    }
}