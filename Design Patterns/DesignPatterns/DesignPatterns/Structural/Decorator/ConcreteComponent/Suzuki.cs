﻿using DesignPatterns.Structural.Decorator.Component;

namespace DesignPatterns.Structural.Decorator.ConcreteComponent
{
    public sealed class Suzuki : ICar
    {
        public string Make
        {
            get { return "Hatchback"; }
        }

        public double GetPrice()
        {
            return 8000000;
        }
    }
}