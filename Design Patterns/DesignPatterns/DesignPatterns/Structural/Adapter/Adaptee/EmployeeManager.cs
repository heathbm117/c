﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DesignPatterns.Structural.Adapter.Adaptee
{
    public class EmployeeManager
    {
        public List<Employee2> employees;

        public EmployeeManager()
        {
            employees = new List<Employee2>();
            this.employees.Add(new Employee2(1, "John"));
            this.employees.Add(new Employee2(2, "Michael"));
            this.employees.Add(new Employee2(3, "Jason"));
        }

        public virtual string GetAllEmployees()
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(employees.GetType());
            var settings = new XmlWriterSettings(); settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, employees, emptyNamepsaces);
                return stream.ToString();
            }
        }
    }
}
