﻿using System;
using System.Xml.Serialization;

namespace DesignPatterns.Structural.Adapter.Adaptee
{
    [Serializable]
    public class Employee2
    {
        Employee2() { }

        public Employee2(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }

        [XmlAttribute]
        public int ID { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
    }
}