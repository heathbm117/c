﻿namespace DesignPatterns.Structural.Adapter.Target
{
    public interface IEmployee
    {
        string GetAllEmployees();
    }
}