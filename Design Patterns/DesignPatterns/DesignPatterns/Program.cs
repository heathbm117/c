﻿using DesignPatterns.Creational;
using DesignPatterns.Creational.AbstractFactory;
using DesignPatterns.Models;
using System;
using DesignPatterns.Creational.Builder;
using DesignPatterns.Creational.Builder.Models;
using DesignPatterns.Creational.Prototype.Models;
using DesignPatterns.Structural.Adapter;
using DesignPatterns.Structural.Adapter.Target;
using DesignPatterns.Structural.Adapter.Adaptee;
using DesignPatterns.Structural.Bridge;
using DesignPatterns.Structural.Bridge.Models;
using DesignPatterns.Structural.Composite.Component;
using DesignPatterns.Structural.Composite.Leaf;
using DesignPatterns.Structural.Composite.Composite;
using DesignPatterns.Structural.Decorator.Component;
using DesignPatterns.Structural.Decorator.ConcreteDecorator;
using DesignPatterns.Structural.Decorator.Decorator;
using DesignPatterns.Structural.Decorator.ConcreteComponent;
using FacadeShoppingLibrary;
using DesignPatterns.Structural.Flyweight;
using DesignPatterns.Structural.Proxy;
using DesignPatterns.Behavioral.Chain_of_responsibility;
using DesignPatterns.Behavioral.Command;
using DesignPatterns.Behavioral.Interpretor;
using System.Collections;
using DesignPatterns.Behavioral.Iterator;
using DesignPatterns.Behavioral.Mediator;
using DesignPatterns.Behavioral.Memento;
using DesignPatterns.Behavioral.Observer;
using DesignPatterns.Behavioral.State;
using DesignPatterns.Behavioral.Strategy;
using DesignPatterns.Behavioral.Template_Method;
using DesignPatterns.Behavioral.Visitor;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creation();
            //Structural();
            Behavioral();

            Console.ReadLine();
        }

        static void Creation()
        {
            // singleton
            Singleton singleton = Singleton.Intance;
            Singleton singleton2 = Singleton.Intance;
            Console.WriteLine("Singleton same instance: " + (singleton == singleton2));

            // factory
            Factory factory = new Factory();
            IBusinessObject businessObject = factory.Create(true);

            // abstract factory

            // Abstract factory #1 // products are used in the client

            AbstractFactory factory1 = new ConcreteFactory1();
            Client client1 = new Client(factory1);
            client1.Run(); // product a will be used

            // Abstract factory #2

            AbstractFactory factory2 = new ConcreteFactory2();
            Client client2 = new Client(factory2);
            client2.Run(); // product b will be used

            // builder .NET optimized

            FluidBuilder fluidBuilder = new FluidBuilder();
            PurchaseOrder purchaseOrder = fluidBuilder
                .WithType("bread")
                .ForCompany("bakery")
                .AtAddress("123 street")
                .WithCost(2)
                .WithQuantity(5);

            purchaseOrder.PrintPurchaseOrder();

            // builder

            VehicleBuilder builder;

            // Create shop with vehicle builders

            Shop shop = new Shop();

            // Construct and display vehicles

            builder = new ScooterBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new CarBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new MotorCycleBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            // prototype

            // shallow

            Employee empJohn = new Employee()
            {
                Id = Guid.NewGuid(),
                Name = "John",
                DepartmentID = 150,
                AddressDetails = new Address()
                {
                    DoorNumber = 10,
                    StreetNumber = 20,
                    Zipcode = 90025,
                    Country = "US"
                }
            };

            Console.WriteLine(empJohn.ToString());

            Employee empSam = (Employee)empJohn.Clone();
            empSam.Name = "Sam Paul";
            empSam.DepartmentID = 151;
            empSam.AddressDetails.StreetNumber = 21;
            empSam.AddressDetails.DoorNumber = 11;

            Console.WriteLine(empSam.ToString());

            Console.WriteLine("Modified Details of John");
            empJohn.AddressDetails.DoorNumber = 30;
            empJohn.AddressDetails.StreetNumber = 40;

            empJohn.DepartmentID = 160;
            Console.WriteLine(empJohn.ToString());
            Console.WriteLine(empSam.ToString());

            Console.WriteLine();

            // deep

            Employee empJohn2 = new Employee()
            {
                Id = Guid.NewGuid(),
                Name = "John",
                DepartmentID = 150,
                AddressDetails = new Address()
                {
                    DoorNumber = 10,
                    StreetNumber = 20,
                    Zipcode = 90025,
                    Country = "US"
                }
            };

            Console.WriteLine(empJohn2.ToString());

            Employee empSam2 = empJohn2.DeepCopy();
            empSam2.Name = "Sam Paul";
            empSam2.DepartmentID = 151;
            empSam2.AddressDetails.StreetNumber = 21;
            empSam2.AddressDetails.DoorNumber = 11;

            Console.WriteLine(empSam2.ToString());

            Console.WriteLine("Modified Details of John");
            empJohn2.AddressDetails.DoorNumber = 30;
            empJohn2.AddressDetails.StreetNumber = 40;

            empJohn2.DepartmentID = 160;
            Console.WriteLine(empJohn2.ToString());
            Console.WriteLine(empSam2.ToString());

        }

        static void Structural()
        {
            // Adapter 

            EmployeeManager employeeManager = new EmployeeManager();
            string value2 = employeeManager.GetAllEmployees();

            IEmployee emp = new EmployeeAdapter();
            string value = emp.GetAllEmployees();

            // Bridge

            Payment order = new CardPayment();

            order._IPaymentSystem = new CitiPaymentSystem();
            order.MakePayment();

            order._IPaymentSystem = new BMOPaymentSystem();
            order.MakePayment();

            // composite

            IEmployee3 John = new Employee3("John", "IT");
            IEmployee3 Mike = new Employee3("Mike", "IT");
            IEmployee3 Jason = new Employee3("Jason", "HR");
            IEmployee3 Eric = new Employee3("Eric", "HR");
            IEmployee3 Henry = new Employee3("Henry", "HR");

            IEmployee3 James = new Manager("James", "IT")
            { SubOrdinates = { John, Mike } };
            IEmployee3 Philip = new Manager("Philip", "HR")
            { SubOrdinates = { Jason, Eric, Henry } };

            IEmployee3 Bob = new Manager("Bob", "Head")
            { SubOrdinates = { James, Philip } };
            Bob.GetDetails(1);

            // Decorator

            ICar car = new Suzuki();
            CarDecorator carDecorator = new OfferPrice(car);

            Console.WriteLine("\nOriginal Price " + carDecorator.GetPrice() + " Offer price" + carDecorator.GetDiscountedPrice());

            // Facade

            IUserOrder userOrder = new UserOrder();
            Console.WriteLine("\nFacade : Start");
            Console.WriteLine("************************************");
            int cartID = userOrder.AddToCart(10, 1);
            int userID = 1234;
            Console.WriteLine("************************************");
            int orderID = userOrder.PlaceOrder(cartID, userID);
            Console.WriteLine("************************************");
            Console.WriteLine("Facade : End CartID = {0}, OrderID = {1}",
                cartID, orderID);

            // Flightweight 

            Console.WriteLine("\nFlyweight : Start");

            // Arbitrary extrinsic state

            int extrinsicstate = 22;

            FlyweightFactory factory = new FlyweightFactory();

            // Work with different flyweight instances

            Flyweight fx = factory.GetFlyweight("X");
            fx.Operation(--extrinsicstate);

            Flyweight fy = factory.GetFlyweight("Y");
            fy.Operation(--extrinsicstate);

            Flyweight fz = factory.GetFlyweight("Z");
            fz.Operation(--extrinsicstate);

            UnsharedConcreteFlyweight fu = new UnsharedConcreteFlyweight();

            fu.Operation(--extrinsicstate);

            // Proxy

            IATM_Machine ATM = new ProxyATM_Machine();
            ATM.GetCashBalance(); // 1000
            ATM.Take20();
            ATM.GetCashBalance(); // 980
        }

        static void Behavioral()
        {
            // Chain of responsibility

            Numbers numbers = new Numbers(1,1,"add");

            IChain chain = new SubNumbers();
            chain.SetNextChain(new AddNumbers());
            chain.Calculate(numbers);

            // Command

            // Create receiver, command, and invoker

            Receiver receiver = new Receiver();
            Command command = new ConcreteCommand(receiver);
            Invoker invoker = new Invoker();

            // Set and execute command

            invoker.SetCommand(command);
            invoker.ExecuteCommand();

            // Interpretor

            Behavioral.Interpretor.Context context = new Behavioral.Interpretor.Context
            {
                word = "hello"
            };

            // Usually a tree 

            ArrayList list = new ArrayList();

            // Populate 'abstract syntax tree' 

            list.Add(new TerminalExpression());
            list.Add(new NonterminalExpression());
            list.Add(new TerminalExpression());
            list.Add(new TerminalExpression());

            // Interpret

            foreach (AbstractExpression exp in list)
            {
                exp.Interpret(context);
            }

            // Iterator

            ConcreteAggregate a = new ConcreteAggregate();
            a[0] = "Item A";
            a[1] = "Item B";
            a[2] = "Item C";
            a[3] = "Item D";

            // Create Iterator and provide aggregate

            Iterator i = a.CreateIterator();

            Console.WriteLine("Iterating over collection:");

            object item = i.First();
            while (item != null)
            {
                Console.WriteLine(item);
                item = i.Next();
            }

            // Mediator

            ConcreteMediator m = new ConcreteMediator();

            ConcreteColleague1 c1 = new ConcreteColleague1(m);
            ConcreteColleague2 c2 = new ConcreteColleague2(m);

            m.Colleague1 = c1;
            m.Colleague2 = c2;

            c1.Send("How are you?");
            c2.Send("Fine, thanks");

            // Memento

            Originator o = new Originator();
            o.State = "On";

            // Store internal state

            Caretaker c = new Caretaker();
            c.Memento = o.CreateMemento();

            // Continue changing originator

            o.State = "Off";

            // Restore saved state

            o.SetMemento(c.Memento);

            // Observer .NET optimised

            TrainSignal trainSignal = new TrainSignal();
            new Car(trainSignal);
            new Car(trainSignal);
            new Car(trainSignal);
            new Car(trainSignal);
            new Car(trainSignal);
            trainSignal.HereComesATrain();

            // State

            // Setup context in a state

            var state = new DesignPatterns.Behavioral.State.Context(new ConcreteStateA());

            // Issue requests, which toggles state

            state.Request();
            state.Request();
            state.Request();
            state.Request();

            // Startegy

            Behavioral.Strategy.Context context5;

            // Three contexts following different strategies

            context5 = new Behavioral.Strategy.Context(new ConcreteStrategyA());
            context5.ContextInterface();

            context5 = new Behavioral.Strategy.Context(new ConcreteStrategyB());
            context5.ContextInterface();

            context5 = new Behavioral.Strategy.Context(new ConcreteStrategyC());
            context5.ContextInterface();

            // Template method

            AbstractClass aA = new ConcreteClassA();
            aA.TemplateMethod();

            AbstractClass aB = new ConcreteClassB();
            aB.TemplateMethod();

            // Visitor

            // Setup structure

            ObjectStructure os = new ObjectStructure();
            os.Attach(new ConcreteElementA());
            os.Attach(new ConcreteElementB());

            // Create visitor objects

            ConcreteVisitor1 v1 = new ConcreteVisitor1();
            ConcreteVisitor2 v2 = new ConcreteVisitor2();

            // Structure accepting visitors

            os.Accept(v1);
            os.Accept(v2);
        }
    }
}
